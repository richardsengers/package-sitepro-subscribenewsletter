    <!-- {% crmpro-form %} -->
    
    <div class="mcp-newsletter">
         <h4>Inschrijven nieuwsbrieven</h4>
        <form action="{{action}}" role="form" class="mcp-newsletter-form" method="post">
            <div class="mcp-newsletter-form-result">
              <p class="has-success bg-success" style="display: none">Gelukt!</p>
              <p class="has-error bg-danger" style="display: none">Mislukt!</p>
            </div>
            <div class="form-group has-feedback">
                <label class="control-label" for="mcp-newsletter-form-achternaam">Uw achternaam</label>
                <input type="text" 
                       data-bv-notempty data-bv-notempty-message="Vul uw achternaam in."
                       name="{{field-lastname}}" class="form-control" id="mcp-newsletter-form-lastname" placeholder="Vul uw achternaam in">
                
            </div>
            <div class="form-group has-feedback">
                <label class="control-label" for="mcp-newsletter-form-email">Uw e-mailadres</label>
                <input type="email" 
                       data-bv-emailaddress="true" data-bv-emailaddress-message="Vul uw e-mailadres in. Bijvoorbeeld uwnaam@voorbeeld.nl."
                       data-bv-notempty data-bv-notempty-message="Vul uw e-mailadres in. Bijvoorbeeld uwnaam@voorbeeld.nl."
                       name="{{field-email}}" class="form-control" id="mcp-newsletter-form-email" placeholder="Vul uw e-mailadres in">
                
            </div>
            <div class="form-group has-feedback">
              <button type="submit" class="btn btn-default">Versturen</button>
            </div>
        </form>
    </div>
    
                  
    <!-- {% end crmpro-form %} -->
