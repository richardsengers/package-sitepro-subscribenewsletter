/*jshint devel:true*/
// // the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {
    'use strict';
    
		var pluginName = 'mcpSubscribeNewsletter';
    
		function Plugin ( element, options ) {
        var defaults = {
          elementClass: 'mcp-newsletter-form',
          resultContainer: 'mcp-newsletter-form-result',
          validatorSettings: {
            verbose: false,
            trigger: 'blur',
            feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            }
          },
          onSuccess: this._onSuccess,
          onError: this._onError
        };
      
        this.element = element;
        
        // set first param to true for deep copy (recursive)
				this.settings = $.extend( true, defaults, options );

				this._defaults = defaults;
				this._name = pluginName;
        this._isDev = (window.location.hostname === 'localhost');

				this.init();
		}

		// Avoid Plugin.prototype conflicts
		$.extend(Plugin.prototype, {
				init: function () {
          this.$form = ($(this.element).prop('tagName').toLowerCase() === 'form') ? $(this.element) : $(this.element).find('.' + this.settings.elementClass).eq(0);
          
          // setup bootstrap validator
          $(this.$form).bootstrapValidator(this.settings.validatorSettings);
          $(this.$form).on('error.form.bv', null, this, this.onErrorForm);
          $(this.$form).on('success.form.bv', null, this, this.onSuccessForm);
        },
        
        onErrorForm: function(e) {
          e.preventDefault();
          
          var $scope = e.data;
          $scope.settings.onError.call($scope);
        },
        
        onSuccessForm: function(e) {
          e.preventDefault();

          var $form = $(e.target),
              $scope = e.data,
              setResult = function(result) {
                if(result.success === true) {
                  
                  $scope.settings.onSuccess.call($scope, result);
                } else {
                  $scope.settings.onError.call($scope, result);
                }
              };
              
          if($scope._isDev) {
            $.get('response/mcpSubscribeNewsletter.json', $form.serialize(), setResult, 'json');
          } else {
            $.post($form.attr('action'), $form.serialize(), setResult, 'json');
          }
        },
        
        _onSuccess: function() {
          $('.' + this.settings.resultContainer + ' p').each(function(index,el) {
            var $el = $(el);
            if($el.hasClass('has-success')) {
              $el.show('fast');
            } else{
              $el.hide('fast');
            }
            
          });
        },
        
        _onError: function() {
          $('.' + this.settings.resultContainer + ' p').each(function(index,el) {
            var $el = $(el);
            if($el.hasClass('has-success')) {
              $el.hide('fast');
            } else{
              $el.show('fast');
            }
          });
        },
        
        getForm: function() {
          return this.$form;
        },
        
        getValidator: function() {
          return this.$form.data('bootstrapValidator');
        }
		});

		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function ( options ) {
				this.each(function() {
						if ( !$.data( this, 'plugin_' + pluginName ) ) {
								$.data( this, 'plugin_' + pluginName, new Plugin( this, options ) );
						}
				});

				// chain jQuery functions
				return this;
		};
})( jQuery, window, document );


