# Sitepro subscribe newsletter

## Setup

include this template to your project using bower.

```
#!bower

bower install git+https://bitbucket.org/richardsengers/package-sitepro-subscribenewsletter#~0.2.3 --save
```

## Howto
Most simple way to make use of this plugin is to at the following code

```
#!javascript
 $('.mcp-newsletter').mcpSubscribeNewsletter();

```
This plugin makes use of the [BootstrapValidator](http://bootstrapvalidator.com/settings/). In order to change the default configuration of BootstrapValidator, all settings are extended in this plugin by the validatorSettings property.
For example

```
#!javascript

$('.mcp-newsletter').mcpSubscribeNewsletter({
    validatorSettings: {
        verbose: true,
        trigger: 'blur'
     }
});

```

would override the BootstrapValidator settings.

## Properties
**validatorSettings** *(object)*: Override default BootstrapValidator settings.

**elementClass** *(string)*: form classname

**resultContainer** *(string)*: container class for the result messages



## Events
**onSuccess**: Change the default 'success trigger' when the form successfully submitted
Example:
```
#!javascript

$('.mcp-newsletter').mcpSubscribeNewsletter({
    validatorSettings: {
        verbose: true,
        rigger: 'blur'
     },
     onSuccess: function(result) {
      // this as a scope for the plugin
      //console.log(this);
      //console.log(result);
     }
});

```

**onError**: Change the default 'error trigger' when the form didn't successfully submitted
Example:
```
#!javascript

$('.mcp-newsletter').mcpSubscribeNewsletter({
    validatorSettings: {
        verbose: true,
        rigger: 'blur'
     },
     onSuccess: function(result) {
      // this as a scope for the plugin
      //console.log(this);
      //console.log(result);
     },
     onError: function(result) {
      // this as a scope for the plugin
      //console.log(this);
      //console.log(result);
     }
});

```